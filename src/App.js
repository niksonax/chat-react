import Chat from "./components/Chat";

const URL = "https://edikdolynskyi.github.io/react_sources/messages.json";

function App() {
  return <Chat url={URL} />;
}

export default App;
