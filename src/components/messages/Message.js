import React from "react";
import "./Message.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart as faHeartOutlined } from "@fortawesome/free-regular-svg-icons";
import { faHeart as faHeartFilled } from "@fortawesome/free-solid-svg-icons";

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLiked: false };
  }

  onLikeHandler = () => {
    this.setState({
      isLiked: !this.state.isLiked,
    });
  };

  render() {
    const date = new Date(this.props.createdAt);
    const time = `${date.getHours()}:${date.getMinutes()}`;

    return (
      <div className="message" id={this.props.id}>
        <div className="message-user-avatar">
          <img src={this.props.avatar} alt="avatar" />
        </div>
        <div className="message-text-container">
          <div className="message-header">
            <div className="message-user-name">{this.props.user}</div>
            <div className="message-time">{time}</div>
          </div>
          <div className="message-text">{this.props.text}</div>
          <div
            className={this.state.isLiked ? "message-liked" : "message-like"}
            onClick={this.onLikeHandler}
          >
            {this.state.isLiked ? (
              <FontAwesomeIcon icon={faHeartFilled} />
            ) : (
              <FontAwesomeIcon icon={faHeartOutlined} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Message;
