import React from "react";
import "./MessageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.sendMessageHandler = this.props.sendMessageHandler.bind(this);
    this.state = {
      message: "",
    };
  }

  onChangeHandler = (e) => {
    this.setState({ message: e.target.value });
  };

  onClickHandler = (e) => {
    const textarea = e.target.parentNode.querySelector("textarea");
    textarea.value = "";
    textarea.focus();
  };

  render() {
    return (
      <div className="message-input">
        <textarea
          className="message-input-text"
          onChange={(e) => this.onChangeHandler(e)}
        ></textarea>
        <button
          className="message-input-button"
          onClick={(e) => {
            this.sendMessageHandler(this.state.message);
            this.onClickHandler(e);
          }}
        >
          Send
        </button>
      </div>
    );
  }
}

export default MessageInput;
