import React from "react";
import "./OwnMessage.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";

class OwnMessage extends React.Component {
  constructor(props) {
    super(props);
    this.deleteMessageHandler = this.props.deleteOwnMessage.bind(this);

    this.state = {
      isEditing: false,
      text: this.props.text,
    };
  }

  onMessageEdit = () => {
    this.setState({ isEditing: true });
  };

  render() {
    const date = new Date(this.props.createdAt);
    const time = `${date.getHours()}:${date.getMinutes()}`;

    return (
      <div className="own-message">
        <div className="message-time">{time}</div>
        <div className="message-text">
          {this.state.isEditing ? (
            <textarea className="message-edit-area"></textarea>
          ) : (
            this.props.text
          )}
        </div>
        <div className="message-tools">
          <span className="message-edit">
            <FontAwesomeIcon icon={faEdit} onClick={() => this.onMessageEdit} />
          </span>
          <span className="message-delete">
            <FontAwesomeIcon
              icon={faTrashAlt}
              onClick={() => this.deleteMessageHandler(this.props.id)}
            />
          </span>
        </div>
      </div>
    );
  }
}

export default OwnMessage;
