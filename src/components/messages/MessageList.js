import React from "react";
import "./MessageList.css";
import Message from "./Message";
import OwnMessage from "./OwnMessage";

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.deleteOwnMessage = this.props.deleteOwnMessage;
    this.state = {
      message: "",
    };
  }

  messagesEndRef = React.createRef();

  scrollToBottom = () => {
    this.el.scrollIntoView({ behavior: "smooth" });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    const allMessages = this.props.messages.concat(this.props.ownMessages);

    const sortedMessages = allMessages.sort((firstMessage, secondMessage) => {
      const fstMsgTime = new Date(firstMessage.createdAt).getTime();
      const scndMsgTime = new Date(secondMessage.createdAt).getTime();
      return fstMsgTime - scndMsgTime;
    });

    const messages = sortedMessages.map((message) => {
      if (message.userId === this.props.userId) {
        return (
          <OwnMessage
            key={message.id}
            id={message.id}
            text={message.text}
            createdAt={message.createdAt}
            deleteOwnMessage={this.deleteOwnMessage}
          />
        );
      }
      return (
        <Message
          key={message.id}
          id={message.id}
          userId={message.userId}
          user={message.user}
          avatar={message.avatar}
          text={message.text}
          createdAt={message.createdAt}
        />
      );
    });

    return (
      <div className="message-list">
        {messages}
        <div className="message-divider"></div>
        <br
          ref={(el) => {
            this.el = el;
          }}
        />
      </div>
    );
  }
}

export default MessageList;
