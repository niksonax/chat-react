import React from "react";
import { v4 as uuidv4 } from "uuid";
import "./Chat.css";
import Header from "./Header";
import MessageList from "./messages/MessageList";
import MessageInput from "./messages/MessageInput";
import Preloader from "./Preloader";

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userId: uuidv4(),
      messages: [],
      users: [],
      ownMessages: [],
    };
  }

  getMessages = async () => {
    const response = await fetch(this.props.url);
    const data = await response.json();
    this.setState({ messages: data });
    this.getUsersCount();
    return data;
  };

  getUsersCount = () => {
    const users = [];
    for (const message of this.state.messages) {
      if (!users.includes(message.userId)) {
        users.push(message.userId);
      }
    }
    this.setState({ users: users });
  };

  createOwnMessage = (text) => {
    const date = new Date(Date.now()).toString();
    const id = uuidv4();
    return { id: id, userId: this.state.userId, text: text, createdAt: date };
  };

  deleteOwnMessage = (messageId) => {
    const ownMessages = this.state.ownMessages;
    for (const message of ownMessages) {
      if (message.id === messageId) {
        const messageIndex = ownMessages.indexOf(message);
        ownMessages.splice(messageIndex, 1);
      }
    }
    this.setState({ ownMessages: ownMessages });
  };

  sendMessageHandler = (message) => {
    if (!message) return;
    const ownMessages = this.state.ownMessages;
    const newMessage = this.createOwnMessage(message);
    ownMessages.push(newMessage);
    this.setState({ ownMessages: ownMessages });
  };

  componentDidMount = () => {
    this.getMessages();
  };

  render() {
    if (this.state.messages.length === 0) return <Preloader />;
    return (
      <div className="chat">
        <Header
          users={this.state.users}
          messages={this.state.messages}
          ownMessages={this.state.ownMessages}
        />
        <MessageList
          userId={this.state.userId}
          messages={this.state.messages}
          ownMessages={this.state.ownMessages}
          deleteOwnMessage={this.deleteOwnMessage}
        />
        <MessageInput sendMessageHandler={this.sendMessageHandler} />
      </div>
    );
  }
}

export default Chat;
